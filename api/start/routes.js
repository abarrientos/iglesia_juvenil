'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    greeting: 'Hello world in JSON'
  }
})

const addPrefixToGroup = group => {
  // Grupo para rutas con prefijo /api/
  group.prefix("api");
  return group;
};

addPrefixToGroup(
  Route.group(() => {
    // Insertar rutas sin protección de autenticación aquí. Rutas que si cualquiera las accede no se pone en peligro la integridad de los datos del sistema
    Route.get("example", "ExampleController.index");
    Route.get("example/:_id", "ExampleController.show");
    Route.delete("example/:_id", "ExampleController.destroy");
    Route.put("example/:_id", "ExampleController.update");
    Route.post("example", "ExampleController.store");
  })
);
