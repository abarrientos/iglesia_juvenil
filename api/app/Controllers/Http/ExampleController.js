'use strict'

const Example = use("App/Models/Example")


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with uploads
 */
class ExampleController {
  async upload ({
    request
  }) {
    const profilePic = request.file('files', {
      types: ['image'],
      size: '2mb'
    })

    var dat = request.only(['dat'])
    dat = JSON.parse(dat.dat)
    const date = new Date().getTime()
    if (Helpers.appRoot('storage/uploads')) {
      await profilePic.move(Helpers.appRoot('storage/uploads'), {
        name: dat.code + '-' + dat.name + '.' + profilePic.extname,
        overwrite: true
      })
    } else {
      mkdirp.sync(`${__dirname}/storage/Excel`)
    }
    const data = {
      name: profilePic.fileName
    }
    if (!profilePic.moved()) {
      return profilePic.error()
    }
    return data
  }
  async getFile ({
    params,
    response
  }) {
    const fileName = params.filename
    response.download(Helpers.appRoot('storage/uploads') + `/${fileName}`)
  }

  /**
   * Show a list of all uploads.
   * GET uploads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({
    request,
    response,
    view
  }) {let users = await Example.all()
  response.send(users) }

  /**
   * Render a form to be used for creating a new upload.
   * GET uploads/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({
    request,
    response,
    view
  }) { }

  /**
   * Create/save a new upload.
   * POST uploads
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({
    request,
    response
  }) {
    const body = await request.only(["name","last_name","email","password","date"])
    console.log(body)
    const example = await Example.create(body)
    response.send(example)
   }

  /**
   * Display a single upload.
   * GET uploads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({
    params,
    request,
    response,
    view
  }) { response.send(await Example.find(params._id)) }

  /**
   * Render a form to update an existing upload.
   * GET uploads/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({
    params,
    request,
    response,
    view
  }) { }

  /**
   * Update upload details.
   * PUT or PATCH uploads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({
    params,
    request,
    response
  }) {
    const body = await request.only(["name","last_name","email","password","date"])
    const example = await Example.find(params._id)
    example.name = body.name
    example.last_name = body.last_name
    example.email = body.email
    example.password = body.password
    example.date = body.date
    await example.save()
    response.send(example)
  }

  /**
   * Delete a upload with id.
   * DELETE uploads/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({
    params,
    request,
    response
  }) {
    const user = (await Example.find(params._id)).delete()
    response.send(user)
   }

}

module.exports = ExampleController
