
const routes = [
  {
    path: '/menu',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '/example',
        component: () => import('pages/registro.vue')
      },
      {
        path: '/example/:_id',
        component: () => import('pages/registro.vue')
      },
      {
        path: '/example2',
        component: () => import('pages/Listado.vue')
      }
    ]
  },
  {
    path: '*',
    redirect: '/example'
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
